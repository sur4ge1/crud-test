import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import {Button, ButtonToolbar} from 'react-bootstrap';
import {AddRowModal} from './AddRowModal';
import {EditRowModal} from './EditRowModal';


export class Users extends Component {
    constructor(props) {
        super(props); 
            this.state = {
                records: [{_id: '12', data:{name: "12", age: 12}}],
                addModalShow: false,
                editodalShow: false,
            }
        }

        componentDidMount() {
            this.refreshList();
        }

        refreshList() {
            fetch('http://178.128.196.163:3000/api/records')
            .then(response => response.json())
            .then(data => {
                this.setState({records: data})
            });
        }

        componentDidUpdate() {
            this.refreshList();
        }
          
        deleteRow(itemid) {
            if(window.confirm('Are u sure?'))
            {
                fetch('http://178.128.196.163:3000/api/records/' + itemid, {
                    method: 'DELETE',
                    header: {
                        'Accept': 'application/json',
                        'Content-type': 'application/json'
                    }
                })
            }
        }  
        
    render() {
        const {records, recid, recname, recage} = this.state;
        let addModalClose = () => this.setState({addModalShow: false});
        let editModalClose = () => this.setState({editModalShow: false});
        return (
            <div>
            <Table className="mt-4" striped bordered hover size = "lg">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    
                    {records.map(rec => 
                        <tr key={rec._id}>
                            <td>{rec._id}</td>
                            <td>{(rec.data === undefined) ? "" : rec.data.name}</td>
                            <td>{(rec.data === undefined) ? "" : rec.data.age}</td>
                            <td>
                                <ButtonToolbar>
                                    <Button
                                     className = "mr-2" 
                                     variant = "info"
                                     onClick={() => this.setState({editModalShow: true, recid: rec._id, recname: (rec.data === undefined) ? "" : rec.data.name, recage: (rec.data === undefined) ? "" : rec.data.age})}
                                    >Edit</Button>
                                    <Button
                                        className="mr-2" variant="danger"
                                        onClick={() => this.deleteRow(rec._id, rec.name, rec.age)}
                                        
                                        >Delete row</Button> 
                                     <EditRowModal
                                        show = {this.state.editModalShow}
                                        onHide = {editModalClose}
                                        recid = {recid}
                                        recname = {recname}
                                        recage = {recage}
                                        
                                     />
                                </ButtonToolbar>
                            </td>
                        </tr>
                        )}
                </tbody>
            </Table>
            <ButtonToolbar>
                <Button varian="primary" onClick={() => this.setState({addModalShow: true})}>Add row</Button>
                <AddRowModal 
                show = {this.state.addModalShow}
                onHide = {addModalClose}
                />        
            </ButtonToolbar>
               
            </div>               
        )
    }
}