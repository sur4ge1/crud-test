
import React, {Component} from 'react';
import {Modal, Button, Row, Col, Form} from 'react-bootstrap';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';

export class AddRowModal extends Component {
    constructor(props) {
        super(props);
        this.state = {snackbaropen: false, snackbarmsg: ''}
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    snackbarClose = (event) => {
        this.setState({snackbaropen: false});
    }

    handleSubmit(event) {
        event.preventDefault();

        fetch('http://178.128.196.163:3000/api/records/', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                _id: null,
                data:{
                    name: event.target.Recname.value,
                    age: event.target.Recage.value
                }
            })
        })
        .then(res=> res.json())
        .then((result) => {
            this.setState({snackbaropen: true, snackbarmsg: "Done!"})
        },
        (error) => {
            this.setState({snackbaropen: true, snackbarmsg: "Failed!"})
        }
        )
    }
    
    render() {

        return(
            <div className="container">
                <Snackbar 
                anchorOrigin = {{vertical: 'top', horizontal: 'center'}}
                open = {this.state.snackbaropen}
                autoHideDuration = {3000}
                onClose = {this.snackbarClose}
                message = {<span id="message-id">{this.state.snackbarmsg}</span>}
                action={[
                   <IconButton
                   key = "close"
                   aria-label = "close"
                   color = "inherit"
                   onClick={this.snackbarClose}
                   >x</IconButton>
                ]} 
                />
        <Modal
        {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
        <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Add row
        </Modal.Title>
        </Modal.Header>
      < Modal.Body>
        
            <Row>
                <Col sm={6}>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group controlId = "Recname">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type = "text"
                                name = "Recname"
                                required
                                placeholder = "Derek"   
                            />
                        </Form.Group>
                        <Form.Group controlId = "Recage">
                            <Form.Label>Age</Form.Label>
                            <Form.Control
                                type = "number"
                                name = "Recage"
                                required
                                placeholder = "18"   
                            />
                        </Form.Group>
                       
                        <Form.Group>
                            <Button varian="primary" type="submit">Add row</Button>
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
        
        </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={this.props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
    </div>
        );     
    }
}