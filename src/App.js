import React from 'react';
import './App.css';

import {Home} from './components/Home';
import {Users} from './components/Users';

function App() {
  return (
    <div className="container">
      <Home/>
      <Users/>
    </div>
  );
}

export default App;
